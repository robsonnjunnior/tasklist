package br.com.api.tasklist.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.tasklist.model.bean.Tasks;
import br.com.api.tasklist.repository.TaskListRepository;


@RestController
public class TaskListController {

	@Autowired
	private TaskListRepository service;
	
	
	@RequestMapping(value = "/api")
	@GetMapping
	public @ResponseBody Iterable<Tasks> findAllTasks() {
		return service.findAll();
	}

	@RequestMapping(value = "/api/{description}")
	@GetMapping
	public @ResponseBody List<Tasks> findByDescription(@PathVariable("description") String description) {
		return service.findDescriptionByDescriptionLike(description);
	}

	@ResponseBody
	@RequestMapping(value = "/api/add", method = RequestMethod.POST)
	public void createTasks(@RequestBody final Tasks tasks) {
	    	         
	    service.save(tasks);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/api/del", method = RequestMethod.POST)
	public void deleteTasks(@RequestBody final Tasks tasks) {
	    	         
	    service.delete(tasks);
	}
	
	@ResponseBody
	@RequestMapping(value = "/api/update", method = RequestMethod.POST)
	public void updateTasks(@RequestBody final Tasks tasks) {
	    	         
	    service.save(tasks);
	}
}
