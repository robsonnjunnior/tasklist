package br.com.api.tasklist.model.bean;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Tasks {

	@Id
	private int id;
	private char done;
	
	@Transient
	private boolean checked;

	private Date createdAt = new Date();
	private String description;

	public Tasks() {
	}

	public Tasks(int id, char done, Date createdAt, String description) {
		super();
		setId(id);
		setDone(done);
		setCreatedAt(createdAt);
		setDescription(description);
	}

	public boolean isChecked() {
		if (this.done == 'S') {
			return true;
		} else {
			return false;
		}
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public char getDone() {
		return done;
	}

	public void setDone(char done) {
		this.done = done;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
