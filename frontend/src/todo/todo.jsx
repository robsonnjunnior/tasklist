import React, {Component} from 'react'
import axios from 'axios'

import PageHeader from '../template/pageHeader'
import TodoForm from './todoForm'
import TodoList from './todoList'

const URL = 'http://localhost:8000/api';

export default class Todo extends Component {
    constructor(props){
      super(props)
      this.state = { description: '', list: [] }

      this.handleChange = this.handleChange.bind(this)
      this.handleAdd = this.handleAdd.bind(this)
      this.handleSearch = this.handleSearch.bind(this)
      this.handleClear = this.handleClear.bind(this)

      this.handleRemove = this.handleRemove.bind(this)
      this.handleMarkAsDone = this.handleMarkAsDone.bind(this)
      this.handleMarkAsPending = this.handleMarkAsPending.bind(this)

      this.refresh()
    }


    refresh(description = ''){
      const search = description ? `/${description}` : ''

      axios.get(`${URL}${search}`)
      .then(resp => this.setState({...this.state, description , list: resp.data}))
    }

    handleSearch(){
      this.refresh(this.state.description)
    }

    handleChange(e){
      this.setState({...this.state, description: e.target.value})
    }

    handleAdd(){
      const description = this.state.description
      const task = {description: description };
      console.log(description)
      axios.post(URL + '/add', task)
      .then(resp => this.refresh())
    }

    handleClear(){
      this.refresh()
    }

    handleRemoveee(todo){
      axios.delete(`${URL}/${todo.id}`)
      .then(resp => this.refresh(this.state.description))
    }

    handleRemove(todo){
      const task = todo

      axios.post(URL + '/del', task)
      .then(resp => this.refresh())
    }

    handleMarkAsDone(todo){
      const task = todo
      task.done = 'S'

      axios.post(URL + '/update', task)
      .then(resp => this.refresh())
    }

    handleMarkAsPending(todo){
      const task = todo
      task.done = 'N'

      axios.post(URL + '/update', task)
      .then(resp => this.refresh())

    }

    render(){
        return (
            <div>
                <PageHeader
                  name='Tarefas'
                  small='Cadastro'/>

                <TodoForm
                  description={this.state.description}
                  handleChange={this.handleChange}
                  handleAdd={this.handleAdd}
                  handleSearch={this.handleSearch}
                  handleClear={this.handleClear}/>

                <TodoList
                  list={this.state.list}
                  handleRemove={this.handleRemove}
                  handleMarkAsDone={this.handleMarkAsDone}
                  handleMarkAsPending={this.handleMarkAsPending}/>
            </div>
        )
    }
}
