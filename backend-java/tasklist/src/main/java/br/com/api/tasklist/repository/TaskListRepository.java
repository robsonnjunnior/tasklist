package br.com.api.tasklist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.api.tasklist.model.bean.Tasks;
import java.lang.String;
import java.util.List;

public interface TaskListRepository extends CrudRepository<Tasks, Long> {
	
	@Query(value="select t from Tasks t where t.description like %:description% ")
	List<Tasks> findDescriptionByDescriptionLike(@Param("description") String description);
	

}
